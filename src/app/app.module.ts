import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }   from './app.component';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ParagraphComponent } from './paragraph/paragraph.component';

@NgModule({
    imports:      [ 
        BrowserModule, 
        FormsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }) 
    ],
    providers: [],
    declarations: [ AppComponent, ParagraphComponent ],
    bootstrap:    [ AppComponent ],
})
export class AppModule { }
export function HttpLoaderFactory(http: HttpClient) : TranslateHttpLoader  {
    return new TranslateHttpLoader(http);
}