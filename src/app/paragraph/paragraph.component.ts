import { Component } from '@angular/core';
import { NgZone } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";

declare function updateAllHeadings() : any;

@Component({
  selector: 'paragraph',
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.css', '../../../node_modules/bootstrap/scss/bootstrap.scss']
})
export class ParagraphComponent {
  constructor(public translate: TranslateService, public zone: NgZone) {
    translate.setDefaultLang('en');
    translate.use('en');
    zone.run(() => {
      setTimeout(() => updateAllHeadings(), 1000);
    });
  }

  changeLanguage(lang: string): void {
    this.translate.use(lang);
  }
}
