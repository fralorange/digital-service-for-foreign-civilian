function jumpTo(id) {
    let target = document.getElementById(id)
    target.scrollIntoView({behavior: 'smooth', block: 'center'});
}

//TO-DO исправить некастомизабельность хедингов 
function updateAllHeadings() {
    $("body").on('DOMSubtreeModified', "#headingSeven", function() {
        var headings = ["headingOne", "headingTwo", "headingThree", "headingFour", "headingFive", "headingSix", "headingSeven"];
        var sidebarContent = document.querySelector(".sidebar").children;
        for (let i = 0; i < sidebarContent.length; i++) {
            sidebarContent[i].innerHTML = document.getElementById(headings[i]).innerHTML;
        }
    });
}